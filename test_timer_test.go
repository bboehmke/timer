// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package timer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestTestTimer_StartStop(t *testing.T) {
	ass := assert.New(t)

	timer := TestTimer{}

	ass.False(timer.IsRunning())
	timer.Start()
	ass.True(timer.IsRunning())
	timer.Start()
	ass.True(timer.IsRunning())

	timer.Stop()
	ass.False(timer.IsRunning())
	timer.Stop()
	ass.False(timer.IsRunning())
}

func TestTestTimer_AddRemoveEntry(t *testing.T) {
	ass := assert.New(t)

	timer := TestTimer{}
	entryA := TimeEntry{Time: 111}
	entryB := TimeEntry{Time: 222}

	timer.RemoveEntry(0)
	timer.AddEntry(entryA)
	ass.Equal([]TimeEntry{entryA}, timer.ListEntries())

	timer.Start()
	ass.True(timer.IsRunning())

	timer.AddEntry(entryB)
	ass.Equal([]TimeEntry{entryA, entryB}, timer.ListEntries())

	timer.RemoveEntry(1)
	ass.Equal([]TimeEntry{entryA}, timer.ListEntries())

	timer.Stop()
	ass.False(timer.IsRunning())

	timer.RemoveEntry(0)
	ass.Equal([]TimeEntry{}, timer.ListEntries())
}

func TestTestTimer_Trigger(t *testing.T) {
	ass := assert.New(t)

	var triggerState interface{}
	triggerFunc := func(state interface{}) {
		triggerState = state
	}
	triggerChan := make(chan interface{}, 1)

	timer := TestTimer{
		TriggerFunc: triggerFunc,
		TriggerChan: triggerChan,
	}

	timer.Trigger(42)
	ass.Equal(42, triggerState)

	var triggerState2 interface{}
	select {
	case triggerState2 = <-triggerChan:
	default:
	}
	ass.Equal(42, triggerState2)
}
