// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package timer

import "time"

// possible weekdays
const (
	Sunday int = 1 << iota
	Monday
	Tuesday
	Wednesday
	Thursday
	Friday
	Saturday

	Everyday = Sunday | Monday | Tuesday | Wednesday | Thursday | Friday | Saturday
)

// TimeEntry holds information about next trigger time
type TimeEntry struct {
	Active bool        `json:"active" yaml:"active"`
	Time   int         `json:"time" yaml:"time"` // seconds since midnight
	Repeat int         `json:"repeat" yaml:"repeat"`
	State  interface{} `json:"state" yaml:"state"`
}

// DaysToWait between today and next trigger action
func (e *TimeEntry) DaysToWait(today time.Weekday) int {
	if e.Repeat == 0 {
		return 0 // same day
	}

	var days int
	for days = 0; days <= 7; days++ {
		if e.Repeat&(1<<uint(today)) > 0 {
			break
		}

		today++
		if today > time.Saturday {
			today = time.Sunday
		}
	}

	return days
}

// Next time for trigger action
func (e *TimeEntry) Next(now time.Time) time.Time {
	// if not active return "invalid" date
	if !e.Active {
		return time.Unix(0, 0)
	}

	// get nano seconds since midnight
	t := int64(now.Hour()*3600+now.Minute()*60+now.Second())*1e9 + int64(now.Nanosecond())

	// check if this entry can trigger today
	weekdayStart := now.Weekday()
	days := 0
	if int64(e.Time)*1e9 < t { // skip today -> time gone
		weekdayStart++
		days++
	}

	// get days until next trigger
	days += e.DaysToWait(weekdayStart)

	return time.Date(
		now.Year(), now.Month(), now.Day()+days,
		0, 0, e.Time, 0, now.Location())
}
