// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package timer

import (
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func repeat2Str(repeat int) string {
	list := make([]string, 0, 7)
	if repeat&(0x01) > 0 {
		list = append(list, "Sun")
	}
	if repeat&(0x02) > 0 {
		list = append(list, "Mon")
	}
	if repeat&(0x04) > 0 {
		list = append(list, "Tue")
	}
	if repeat&(0x08) > 0 {
		list = append(list, "Wed")
	}
	if repeat&(0x10) > 0 {
		list = append(list, "Thu")
	}
	if repeat&(0x20) > 0 {
		list = append(list, "Fri")
	}
	if repeat&(0x40) > 0 {
		list = append(list, "Sat")
	}

	return strings.Join(list, "-")
}

var daysToWaitData = []struct {
	today time.Weekday

	days map[int]int
}{
	{time.Sunday, map[int]int{
		Sunday:    0,
		Monday:    1,
		Tuesday:   2,
		Wednesday: 3,
		Thursday:  4,
		Friday:    5,
		Saturday:  6,
	}},
	{time.Monday, map[int]int{
		Sunday:    6,
		Monday:    0,
		Tuesday:   1,
		Wednesday: 2,
		Thursday:  3,
		Friday:    4,
		Saturday:  5,
	}},
	{time.Tuesday, map[int]int{
		Sunday:    5,
		Monday:    6,
		Tuesday:   0,
		Wednesday: 1,
		Thursday:  2,
		Friday:    3,
		Saturday:  4,
	}},
	{time.Wednesday, map[int]int{
		Sunday:    4,
		Monday:    5,
		Tuesday:   6,
		Wednesday: 0,
		Thursday:  1,
		Friday:    2,
		Saturday:  3,
	}},
	{time.Thursday, map[int]int{
		Sunday:    3,
		Monday:    4,
		Tuesday:   5,
		Wednesday: 6,
		Thursday:  0,
		Friday:    1,
		Saturday:  2,
	}},
	{time.Friday, map[int]int{
		Sunday:    2,
		Monday:    3,
		Tuesday:   4,
		Wednesday: 5,
		Thursday:  6,
		Friday:    0,
		Saturday:  1,
	}},
	{time.Saturday, map[int]int{
		Sunday:    1,
		Monday:    2,
		Tuesday:   3,
		Wednesday: 4,
		Thursday:  5,
		Friday:    6,
		Saturday:  0,
	}},
}

func TestTimeEntry_DaysToWait(t *testing.T) {
	for _, entry := range daysToWaitData {
		t.Run(entry.today.String(), func(st *testing.T) {
			for repeat, days := range entry.days {
				st.Run(repeat2Str(repeat), func(sst *testing.T) {
					ass := assert.New(sst)

					e := TimeEntry{
						Repeat: repeat,
					}
					ass.Equal(days, e.DaysToWait(entry.today))
				})
			}
		})
	}
}

func TestTimeEntry_DaysToWaitZero(t *testing.T) {
	entry := TimeEntry{}
	for i := time.Sunday; i <= time.Saturday; i++ {
		t.Run(i.String(), func(st *testing.T) {
			ass := assert.New(st)

			entry.Repeat = 0
			ass.Equal(0, entry.DaysToWait(i))

			entry.Repeat = Everyday
			ass.Equal(0, entry.DaysToWait(i))
		})
	}

}

// note: 2000-01-02 -> sunday
var nextTimeEntryData = []struct {
	name        string
	now         string
	entryRepeat int
	entryTime   int
	nextTime    string
}{
	{
		"next_now",
		"2000-01-02T00:00:00",
		0, 0,
		"2000-01-02T00:00:00",
	},
	{
		"next_today_12",
		"2000-01-02T00:00:00",
		0, 60 * 60 * 12,
		"2000-01-02T12:00:00",
	},
	{
		"next_day",
		"2000-01-02T00:00:00",
		Monday, 0,
		"2000-01-03T00:00:00",
	},
	{
		"next_day_12",
		"2000-01-02T00:00:00",
		Monday, 60 * 60 * 12,
		"2000-01-03T12:00:00",
	},
	{
		"next_day_midnight",
		"2000-01-02T12:00:00",
		0, 0,
		"2000-01-03T00:00:00",
	},
	{
		"next_week_midnight",
		"2000-01-02T12:00:00",
		Sunday, 0,
		"2000-01-09T00:00:00",
	},
	{
		"next_day_new_month",
		"2000-01-31T12:00:00",
		0, 0,
		"2000-02-01T00:00:00",
	},
}

func TestTimeEntry_Next(t *testing.T) {
	timeFormat := "2006-01-02T15:04:05"

	for _, entry := range nextTimeEntryData {
		t.Run(entry.name, func(st *testing.T) {
			ass := assert.New(st)

			now, err := time.Parse(timeFormat, entry.now)
			ass.NoError(err)

			e := TimeEntry{
				Active: true,
				Repeat: entry.entryRepeat,
				Time:   entry.entryTime,
			}
			ass.Equal(entry.nextTime, e.Next(now).Format(timeFormat))
		})
	}
}

func TestTimeEntry_NextInvalid(t *testing.T) {
	ass := assert.New(t)

	e := TimeEntry{}
	ass.Equal(int64(0), e.Next(time.Now()).Unix())
}
