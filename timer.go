// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package timer

import (
	"sort"
	"sync"
	"time"
)

// NewTimer creates a new timer object with a trigger callback and a trigger channel
func NewTimer(f TriggerFunc, c chan interface{}, times []*TimeEntry) Timer {
	if times == nil {
		times = make([]*TimeEntry, 0)
	}
	return &timer{
		times: times,

		stop:    make(chan struct{}),
		add:     make(chan TimeEntry),
		remove:  make(chan int),
		entries: make(chan chan []TimeEntry),

		triggerFunc: f,
		triggerChan: c,
	}
}

// NewTimer creates a new timer object with a trigger callback
func NewTimerFunc(f TriggerFunc, times []*TimeEntry) Timer {
	return NewTimer(f, nil, times)
}

// NewTimer creates a new timer object with a trigger channel
func NewTimerChan(c chan interface{}, times []*TimeEntry) Timer {
	return NewTimer(nil, c, times)
}

// TriggerFunc to handle timer trigger events
type TriggerFunc func(interface{})

// Timer defines the interface provided by a timer
type Timer interface {
	// Start the timer handling
	Start()

	// Stop the timer handling
	Stop()

	// IsRunning returns true if the timer is running
	IsRunning() bool

	// AddEntry to timer
	AddEntry(entry TimeEntry)

	// RemoveEntry from timer
	RemoveEntry(idx int)

	// ListEntries of timer
	ListEntries() []TimeEntry
}

// timer triggers on time entries
type timer struct {
	// list of time entries
	times []*TimeEntry

	// channel to communicate with run function
	stop    chan struct{}
	add     chan TimeEntry
	remove  chan int
	entries chan chan []TimeEntry

	// running state
	running      bool
	runningMutex sync.RWMutex

	// handler for trigger
	triggerFunc TriggerFunc
	triggerChan chan interface{}
}

// Start the timer handling
func (t *timer) Start() {
	t.runningMutex.Lock()
	defer t.runningMutex.Unlock()

	if t.running {
		return
	}
	t.running = true

	go t.run()
}

// Stop the timer handling
func (t *timer) Stop() {
	t.runningMutex.Lock()
	defer t.runningMutex.Unlock()

	if !t.running {
		return
	}
	t.running = false

	t.stop <- struct{}{}
}

// IsRunning returns true if the timer is running
func (t *timer) IsRunning() bool {
	t.runningMutex.RLock()
	defer t.runningMutex.RUnlock()

	return t.running
}

// AddEntry to timer
func (t *timer) AddEntry(entry TimeEntry) {
	t.runningMutex.RLock()
	defer t.runningMutex.RUnlock()

	// not running -> direct call internal function
	if t.running {
		t.add <- entry
	} else {
		t.addEntry(entry)
	}
}

// addEntry to timer (internal)
func (t *timer) addEntry(entry TimeEntry) {
	t.times = append(t.times, &entry)
	sort.Slice(t.times, func(i, j int) bool {
		return t.times[i].Time < t.times[j].Time
	})
}

// RemoveEntry from timer
func (t *timer) RemoveEntry(idx int) {
	t.runningMutex.RLock()
	defer t.runningMutex.RUnlock()

	// not running -> direct call internal function
	if t.running {
		t.remove <- idx
	} else {
		t.removeEntry(idx)
	}
}

// removeEntry from timer (internal)
func (t *timer) removeEntry(idx int) {
	t.times = append(t.times[:idx], t.times[idx+1:]...)
}

// ListEntries of timer
func (t *timer) ListEntries() []TimeEntry {
	t.runningMutex.RLock()
	defer t.runningMutex.RUnlock()

	// not running -> direct call internal function
	if !t.running {
		return t.listEntries()
	}

	response := make(chan []TimeEntry, 1)
	t.entries <- response

	select {
	case entries := <-response:
		return entries
	case <-time.After(time.Second):
		return make([]TimeEntry, 0)
	}
}

// listEntries of this timer (internal)
func (t *timer) listEntries() []TimeEntry {
	entries := make([]TimeEntry, len(t.times))
	for idx, entry := range t.times {
		entries[idx] = *entry
	}
	return entries
}

// run main loop
func (t *timer) run() {
	now := time.Now()
	for {
		var nextEntries []*TimeEntry
		nextTime := now.AddDate(0, 1, 0)

		// get next entries
		for _, entry := range t.times {
			if !entry.Active {
				continue
			}

			et := entry.Next(now)
			sub := et.Sub(nextTime)
			if sub < 0 {
				nextEntries = []*TimeEntry{entry}
				nextTime = et
			} else if sub == 0 {
				nextEntries = append(nextEntries, entry)
			}
		}

		// check if timer found
		var timer *time.Timer
		if len(nextEntries) == 0 {
			timer = time.NewTimer(100000 * time.Hour)
		} else {
			timer = time.NewTimer(nextTime.Sub(now))
		}

		select {
		case now = <-timer.C: // timer triggered
			t.triggerEntries(nextEntries)

		case entry := <-t.add: // add new time entry
			timer.Stop()
			now = time.Now()

			t.addEntry(entry)

		case idx := <-t.remove: // remove time entry
			timer.Stop()
			now = time.Now()

			t.removeEntry(idx)

		case response := <-t.entries: // handle request of entry list
			now = time.Now()
			response <- t.listEntries()

		case <-t.stop: // stop main loop
			timer.Stop()
			return
		}
	}
}

// triggerEntries if given and active
func (t *timer) triggerEntries(entries []*TimeEntry) {
	if len(entries) > 0 {
		for _, entry := range entries {
			if entry.Repeat == 0 {
				entry.Active = false
			}

			if t.triggerFunc != nil {
				t.triggerFunc(entry.State)
			}

			if t.triggerChan != nil {
				t.triggerChan <- entry.State
			}
		}
	}
}
