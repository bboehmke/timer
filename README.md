# timer

Simple timer/time switch implementations for go.

This module can be used for simple ON/OFF time switch applications or 
more complex application with various values for each trigger event.

The implementation based on https://github.com/robfig/cron

## Usage

Create a timer instance with a callback function and/or a channel that handles 
triggered time entries.
```go
// function used to handle trigger of time entries
triggerFunc := func(state interface{}) {
	// handle timer trigger
}

// channel used to handle trigger of time entries
triggerChan := make(chan interface{}, 1)

// create time
timer := NewTimer(handler, triggerChan, nil)
```

Add one or more time entries to the timer:
```go
timer.AddEntry(TimeEntry{
    Active: True,
    Repeat: timer.Everyday,
    Time: 60*60*8, // time in seconds since midnight
})
```

Start the timer and wait for the trigger event:
```go
timer.Start()
```

Additional entries can be added and removed while the timer is running.
