// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package timer

// TestTimer for easier unit testing
type TestTimer struct {
	Times       []TimeEntry
	TriggerFunc TriggerFunc
	TriggerChan chan interface{}

	Running bool
}

// Start the timer handling
func (t *TestTimer) Start() {
	t.Running = true
}

// Stop the timer handling
func (t *TestTimer) Stop() {
	t.Running = false
}

// IsRunning returns true if the timer is running
func (t *TestTimer) IsRunning() bool {
	return t.Running
}

// AddEntry to timer
func (t *TestTimer) AddEntry(entry TimeEntry) {
	if t.Times == nil {
		t.Times = make([]TimeEntry, 0)
	}

	t.Times = append(t.Times, entry)
}

// RemoveEntry from timer
func (t *TestTimer) RemoveEntry(idx int) {
	if t.Times == nil {
		return
	}
	t.Times = append(t.Times[:idx], t.Times[idx+1:]...)
}

// ListEntries of timer
func (t *TestTimer) ListEntries() []TimeEntry {
	return t.Times
}

// Trigger callback function and channel
func (t *TestTimer) Trigger(state interface{}) {
	if t.TriggerFunc != nil {
		t.TriggerFunc(state)
	}

	if t.TriggerChan != nil {
		t.TriggerChan <- state
	}
}
