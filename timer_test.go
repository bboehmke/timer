// Copyright 2020 Benjamin Böhmke <benjamin@boehmke.net>.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package timer

import (
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewTimer(t *testing.T) {
	ass := assert.New(t)

	triggerFunc := func(state interface{}) {}
	triggerChan := make(chan interface{})
	times := []*TimeEntry{
		{
			Time: 111,
		},
		{
			Time: 222,
		},
	}

	ti := NewTimer(triggerFunc, triggerChan, nil).(*timer)
	ass.NotNil(ti.triggerFunc)
	ass.Equal(triggerChan, ti.triggerChan)
	ass.NotNil(ti.times)
	ass.Len(ti.times, 0)
	ass.NotNil(ti.stop)
	ass.NotNil(ti.add)
	ass.NotNil(ti.remove)
	ass.NotNil(ti.entries)

	ti = NewTimerFunc(triggerFunc, nil).(*timer)
	ass.NotNil(ti.triggerFunc)
	ass.Nil(ti.triggerChan)
	ass.NotNil(ti.times)
	ass.Len(ti.times, 0)
	ass.NotNil(ti.stop)
	ass.NotNil(ti.add)
	ass.NotNil(ti.remove)
	ass.NotNil(ti.entries)

	ti = NewTimerChan(triggerChan, nil).(*timer)
	ass.Nil(ti.triggerFunc)
	ass.Equal(triggerChan, ti.triggerChan)
	ass.NotNil(ti.times)
	ass.Len(ti.times, 0)
	ass.NotNil(ti.stop)
	ass.NotNil(ti.add)
	ass.NotNil(ti.remove)
	ass.NotNil(ti.entries)

	ti = NewTimer(triggerFunc, triggerChan, times).(*timer)
	ass.NotNil(ti.triggerFunc)
	ass.Equal(triggerChan, ti.triggerChan)
	ass.Equal(times, ti.times)
	ass.NotNil(ti.stop)
	ass.NotNil(ti.add)
	ass.NotNil(ti.remove)
	ass.NotNil(ti.entries)
}

func TestTimer_StartStop(t *testing.T) {
	ass := assert.New(t)

	timer := NewTimer(nil, nil, nil).(*timer)

	ass.False(timer.IsRunning())
	timer.Start()
	ass.True(timer.IsRunning())
	timer.Start()
	ass.True(timer.IsRunning())

	timer.Stop()
	ass.False(timer.IsRunning())
	timer.Stop()
	ass.False(timer.IsRunning())
}

func TestTimer_AddRemoveEntry(t *testing.T) {
	ass := assert.New(t)

	timer := NewTimer(nil, nil, nil).(*timer)
	entryA := TimeEntry{Time: 111}
	entryB := TimeEntry{Time: 222}

	timer.AddEntry(entryA)
	ass.Equal([]TimeEntry{entryA}, timer.ListEntries())

	timer.Start()
	ass.True(timer.IsRunning())

	timer.AddEntry(entryB)
	ass.Equal([]TimeEntry{entryA, entryB}, timer.ListEntries())

	timer.RemoveEntry(1)
	ass.Equal([]TimeEntry{entryA}, timer.ListEntries())

	timer.Stop()
	ass.False(timer.IsRunning())

	timer.RemoveEntry(0)
	ass.Equal([]TimeEntry{}, timer.ListEntries())
}

func TestTimer_trigger(t *testing.T) {
	ass := assert.New(t)

	var triggerTime time.Time
	var triggerState interface{}
	var mutex sync.Mutex
	triggerFunc := func(state interface{}) {
		mutex.Lock()
		defer mutex.Unlock()
		triggerTime = time.Now()
		triggerState = state
	}
	triggerChan := make(chan interface{}, 1)

	now := time.Now()
	timer := NewTimer(triggerFunc, triggerChan, []*TimeEntry{
		{
			Active: true,
			Time:   (now.Hour()*60+now.Minute())*60 + now.Second() + 1,
			State:  42,
		},
		{
			Active: true,
			Time:   (now.Hour()*60+now.Minute())*60 + now.Second() + 1,
			State:  42,
		},
	}).(*timer)

	timer.Start()
	ass.True(timer.running)
	defer timer.Stop()

	time.Sleep(time.Second * 2)
	mutex.Lock()
	defer mutex.Unlock()
	ass.True(triggerTime.Sub(now) <= time.Second*2)
	ass.Equal(42, triggerState)

	var triggerState2 interface{}
	select {
	case triggerState2 = <-triggerChan:
	default:
	}
	ass.Equal(42, triggerState2)

	ass.Equal([]TimeEntry{
		{
			Active: false,
			Time:   (now.Hour()*60+now.Minute())*60 + now.Second() + 1,
			State:  42,
		},
		{
			Active: false,
			Time:   (now.Hour()*60+now.Minute())*60 + now.Second() + 1,
			State:  42,
		},
	}, timer.ListEntries())
}
